import java.util.Scanner;
public class Firstexercise {
	public static void main(String[] args) {
		String str1 = "hot";
		String str2 = "dog";
		System.out.println(str1 + str2);
		
		System.out.println("The greatest number is " + getGreatestNum(23, 12, 19));
		System.out.println("The greatest number is " + getGreatestNum(122, 98, 377));
		
		System.out.println(printFibonacciNum(8));
		System.out.println(printFibonacciNum(25));
		
		Scanner in = new Scanner(System.in);
		System.out.println("Type 4 numbers");
		int x = in.nextInt();
		int y = in.nextInt();
		int z = in.nextInt();
		int w = in.nextInt();
		System.out.println("The greatest number is " + getGreatestNum(x, y, z, w));
		
		
	}
	public static int getGreatestNum(int x, int y, int z){
		int num = Math.max(x,Math.max(y, z));
		return num;
	}
	public static int printFibonacciNum(int count) {
		int a = 0;
		int b = 1;
		int c = 0;
		
		for (int i = 0; i < count; i++) {
			c = a + b;
			a = b;
			b = c;
		}
		return c;
	}
	public static int getGreatestNum(int x, int y, int z, int w) {
		int num = Math.max(x, Math.max(y, Math.max(z, w)));
		return num;
	}

}

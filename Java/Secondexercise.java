import java.util.Scanner;
public class Secondexercise {
	
	public static void main(String[] args) {
		int n = 5;
		for (int i = 2; i <= n + 1; i++) {
			System.out.println(fib(i));
		}
	}

	public static long fib(int n) {
		
		if (n == 0 || n == 1) {
			return n;
		}
		else {
			long fibrec = (fib(n - 1) + fib(n - 2));
			return fibrec;
		}
	}

}

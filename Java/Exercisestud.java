
public class Exercisestud {
	public static void main(String[] args) {
		Student Gustavo = new Student("Gustavo", "Osuna", "Lugo");
		Student Pablo = new Student("Pablo", "Rodriguez", "Gomez");
		Student Efren = new Student("Efren", "Landell", "Arellano");
		
		System.out.println(Gustavo.StudentId);
		System.out.println(Pablo.StudentId);
		System.out.println(Efren.StudentId);
		
		Gustavo.concat();
		Pablo.concat();
		Efren.concat();
		
		Gustavo.setBirth_date("28/01/2000");
		Pablo.setBirth_date("09/09/2001");
		Efren.setBirth_date("11/11/1999");
		
		System.out.println(Gustavo.getBirth_date());
		System.out.println(Pablo.getBirth_date());
		System.out.println(Efren.getBirth_date());
	}

}

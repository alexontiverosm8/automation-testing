import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Student {
	int StudentId;
	String FirstName;
	String MidlleName;
	String LastName;
	String DOB;
	
	public Student(String FirstName, String MidlleName, String LastName) {
		this.StudentId = (int) (Math.random()*(1000-1)) + 1;
		this.FirstName = FirstName;
		this.MidlleName = MidlleName;
		this.LastName = LastName;
	}
	public String getBirth_date() {
		return DOB;
	}
	public void setBirth_date(String DOB) {
		String oldstring = DOB + " 00:00:00.0";
		LocalDateTime datetime = LocalDateTime.parse(oldstring, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss.S"));
		String newString = datetime.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
		this.DOB = newString;
	}
	public void concat() {
		System.out.println(this.FirstName + this.MidlleName + this.LastName);
	}
}

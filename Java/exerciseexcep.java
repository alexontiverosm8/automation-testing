import java.util.Scanner;
public class exerciseexcep {
	public static void main(String[] args) {
		System.out.println("Type two numbers: ");
		Scanner in = new Scanner(System.in);
		double firstnum = in.nextInt();
		double secnum = in.nextInt();
		excHandlingNumber(firstnum,secnum);
	}
		
		public static void excHandlingNumber(double firstnum, double secnum) {
			try {
				double result = firstnum/secnum;
				System.out.println("The result of the divison is: " + result);
			}
			catch (ArithmeticException e) {
				System.out.println("The result of the division is: Infinity");
			}
		}
}

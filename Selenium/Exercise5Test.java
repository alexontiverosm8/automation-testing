package pages;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Exercise5Test {

	@Test
	public void testexercisenames() {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\KB\\webecli\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
		Exercise5 form = new Exercise5();
		form.datasubmit(driver);
		WebDriverWait wait = new WebDriverWait(driver,5000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		
		assertEquals(driver.findElement(By.cssSelector("[align='center']")).getText(), "Thanks for submitting your form");
		System.out.println("Test successful");
	}
	}



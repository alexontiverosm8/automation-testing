package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebFormPage {
	public static void submitForm(WebDriver driver) {
		driver.findElement(By.id("first-name")).sendKeys("Alejandro");
		driver.findElement(By.id("last-name")).sendKeys("Ontiveros Magallanes");
		driver.findElement(By.id("job-title")).sendKeys("Developer");
		WebElement rbutton =driver.findElement(By.id("radio-button-2"));
		rbutton.click();
		WebElement cbox = driver.findElement(By.id("checkbox-1"));
		cbox.click();
		WebElement ymenu = driver.findElement(By.id("select-menu"));
		ymenu.click();
		WebElement yexperience = driver.findElement(By.cssSelector("option[value='1'"));
		yexperience.click();
		WebElement dpicker = driver.findElement(By.id("datepicker"));
		dpicker.sendKeys("01/06/2022");
		dpicker.sendKeys(Keys.RETURN);
		WebElement sbutton = driver.findElement(By.cssSelector("a[role='button']"));
		sbutton.click();
	}
}

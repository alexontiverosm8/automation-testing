import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Exercise2 {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\KB\\webecli\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://formy-project.herokuapp.com/form");
		WebElement fname =driver.findElement(By.id("first-name"));
		fname.sendKeys("Alejandro");
		WebElement lname =driver.findElement(By.id("last-name"));
		lname.sendKeys("Ontiveros Magallanes");
		WebElement jtitle =driver.findElement(By.id("job-title"));
		jtitle.sendKeys("Developer");
		WebElement rbutton =driver.findElement(By.id("radio-button-2"));
		rbutton.click();
		WebElement cbox = driver.findElement(By.id("checkbox-1"));
		cbox.click();
		WebElement ymenu = driver.findElement(By.id("select-menu"));
		ymenu.click();
		WebElement yexperience = driver.findElement(By.cssSelector("option[value='1'"));
		yexperience.click();
		WebElement dpicker = driver.findElement(By.id("datepicker"));
		dpicker.sendKeys("01/06/2022");
		dpicker.sendKeys(Keys.RETURN);
		WebElement sbutton = driver.findElement(By.cssSelector("a[role='button']"));
		sbutton.click();
	}

}

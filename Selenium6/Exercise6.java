package pages;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.excel.lib.util.Xls_Reader;

public class Exercise6 {
	public static void screenexercise(WebDriver driver) throws IOException {
		Xls_Reader reader = new Xls_Reader("C:\\Users\\KB\\eclipse-workspace\\Automation\\src\\Samplenames.xlsx");
		String sheetname = "Hoja1";
		
		//Locators
		WebElement fname =driver.findElement(By.id("first-name"));
		WebElement lname =driver.findElement(By.id("last-name"));
		WebElement jtitle =driver.findElement(By.id("job-title"));
		
		int rowNum = 2;
		String firname = reader.getCellData(sheetname, "FirstName", rowNum);
		String lasname = reader.getCellData(sheetname, "LastName", rowNum);
		String job = reader.getCellData(sheetname, "JobTitle", rowNum);
		String datetoday = reader.getCellData(sheetname, "Date", rowNum);
		
		fname.sendKeys(firname);
		lname.sendKeys(lasname);
		jtitle.sendKeys(job);
		
		WebElement rbutton =driver.findElement(By.id("radio-button-2"));
		rbutton.click();
		
		//Checkbox screenshot
		WebElement cbox = driver.findElement(By.id("checkbox-1"));
		cbox.click();
		WebElement fullcbox = driver.findElement(By.xpath("/html/body/div/form/div/div[5]"));
		File elementboxscreen = fullcbox.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(elementboxscreen, new File("checkboxmale.png"));
		
		
		//In select screenshot
		WebElement ymenu = driver.findElement(By.id("select-menu"));
		ymenu.click();
		WebElement yexperience = driver.findElement(By.cssSelector("option[value='1'"));
		yexperience.click();
		WebElement fullyear = driver.findElement(By.xpath("/html/body/div/form/div/div[6]"));
		File elementyear = fullyear.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(elementyear, new File("yearsofexperience.png"));
		
		
		//Datepicker screenshot
		
		WebElement dpicker = driver.findElement(By.id("datepicker"));
		dpicker.sendKeys(datetoday);
		dpicker.sendKeys(Keys.RETURN);
		WebElement fulldate = driver.findElement(By.xpath("/html/body/div/form/div/div[7]"));
		File elementdate = fulldate.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(elementdate, new File("todaydate.png"));
		
		//Submit
		WebElement sbutton = driver.findElement(By.cssSelector("a[role='button']"));
		sbutton.click();
		
		
	}

}
